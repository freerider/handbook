# Freeriders' Privacy (TTC Edition)

---

## ISSUES OF PRESTO PRIVACY

### Mind the METADATA

Metadata is often described as everything except the content of your activities. You can think of metadata as the equivalent of an envelope. Just like an envelope contains information about the sender, receiver, and destination of a message, so does metadata. Metadata is information about the activities you interact. Some examples of metadata include:

 - Timestamp of your travel
 - Location of your travel interaction
 - Card's serial number or Unique ID
 - Duration of your travel
 - Who or what you interact on the travel duration

Quote the [CCLA](https://ccla.org/privacy/surveillance-technology/presto-change-o-privacy-disappears/):

> `Presto is optimized to operate for identified users.` When we become an account holder, benefits include the ability to cancel a card if it’s lost, the ability to check to confirm we’re only getting charged for the trips we’ve taken, and the ability to automatically re-load a card by linking it to a credit card. But all that convenience comes at a cost. Every ride is tracked. Every time we tap the card our presence is recorded. That information can be [`shared with police`](https://archive.ph/PECvy), transit safety officers or special constables, without a warrant in many cases. **The records are stored for at least five years,** according to the Presto terms of service. And to be clear, much of the data collection is not a necessity, it’s a design choice. Many of the benefits—including online reload and reviewing charges—could be possible without recording your location every time you tap, that’s just not the way the system was designed.

About the [warrantless Presto access sharing](https://www.prestocard.ca/en/privacy) with police and state:

> Under FIPPA, in certain circumstances, Metrolinx may share personal information of its PRESTO customers in response to a reasonable request from a police officer, special constable, or transit safety officer (“officers”). In such circumstances, `Metrolinx may choose to share information without requiring a warrant or court order.` These circumstances include:

> 1. where there are immediate and compelling concerns about an individual’s health or safety, such as a lost or missing person, and there appears to be no other way for the official to obtain the requested information in a timely manner;
> 2. in an emergency, to facilitate contact with a spouse or relative, such as where a person has been injured or is ill, and where the delay in providing the information could be harmful to someone’s health or safety; or
> 3. where a PRESTO transit operator is investigating a safety or security incident, such as theft, vandalism, assault or other offence , on or in relation to the transit operator’s property or services.

### Metadata on a Presto Card

#### EXTERNAL

    Date issued: [00/00/0000]
	Activation ID: 3 numbers [000]
	Card ID: 17 numbers [000000 0000 0000 000]

#### NFC

![](https://i.imgur.com/Uj3VLo6.png)

**Spec:**

    [ISO 1443-4] MIFARE DESFire EV1 4k
	IsoDep, NfcA, NdefFormatable

**Trackable/Changeable:**

    Serial Number
	UID
	Created and Modification Timestamps
	Last Used Timestamps

### Lookout for LINKABILITY

Linkability is when amassed pieces of metadata `link` to a targeted person of interest.

To put this in a more practical example, let's map the [threat models](https://ssd.eff.org/en/glossary/threat-model) of Presto card:

 1. How would the method you purchase and refill your Presto can be tracked?

   - Bank card(s)
   - Name (if registered)
   - Timestamp and location of your purchase/refill
 
 2. What kind of personal metadata can Presto track?
 
   - Name (if registered)
   - Email (if registered)
   - Password (if registered)
   - Security question(s) (if registered)
   - IP Address (if registered)
   - Device used to access the account (if registered)

#### To repeat the PRESTO PRIVACY 101

 - `NEVER LINK YOUR IDENTITY TO THE PRESTO CARD`
 - `PAY AND REFILL YOUR PRESTO WITH CASH`
 - `WHEN NECESSARY, SWITCH IT ONCE A WHILE`

---

## MAINTAINING PRIVACY ON TTC

 - The trio starter privacy tools are: `Mask, sunglasses and hat`
 - Refrain from talking in the station. The Staff have Ears.
 - Change your routes to alternative once a while if you are a frequent freerider.
 - If pursued, catch a hike to random safe station with rear entrance and exit there.
 - When in doubt, a fine sweep of mini laser pointer at the cam create 5-sec blindspot.

### SUBWAY STATION SECURITY

![](https://toronto.citynews.ca/wp-content/blogs.dir/sites/10/2018/11/30/ttc-delays1-768x576.jpg)

The places most installed cams in a station is `ticket booths` and `streetcar platforms`

Ironically the most needed place often have the least cams: `bus platforms`

All terminal stations have on-site security personnel, but the ones that don't, have `station supervisor(s)` and mobile `Special Constables` that would show up when an internal station incident happen.

![](https://i0.wp.com/media.globalnews.ca/videostatic/692/595/GTNH01032017_TTCOFFICERS_tnb_2.jpg)
 
Secondary TTC staff like janitor(s) or maintenance would also snitch out freeriders or troublemakers, as per TTC policies that they collaborate "in-full" with police and transit security. These snitching incidents often can be heard on the [Toronto Transit Control](https://www.broadcastify.com/webPlayer/31629) that station staff collaborated with security to bait the person of interest into entrances that waited by TPS.

##### `TTC supervision and security DO NOT GIVE UP pursuing the person of interest until they leave the TTC properties. As long as a POI still remains on TTC property they would be pursued. There have been incidents where Transit Control coordinated with police and security to catch a subway tunnel adventurist, that involved TPS to arrest the POI as soon as they left the properties.`
 
##### **`TRUST NO ONE`**

![](https://images.thestar.com/sS1mdcGl1ljqdlYKiICYSYqvD2k=/650x650/smart/filters:cb(2700061000):format(webp)/https://www.thestar.com/content/dam/localcommunities/scarborough_mirror/news/2020/12/29/ttc-station-janitor-s-top-priority-is-to-protect-riders-from-covid-19/CCDragicaKuzmanovska2.JPG)
 
### TRANSIT ENFORCEMENT UNIT

![](https://media.blogto.com/articles/20190523-ttc-fare-evasion.jpg?w=1200&cmd=resize_then_crop&height=630&quality=7)
 
TTC pork is consisted of 2 part, Special Constables (subway pigs) and Fare Inspectors (wannabe pigs)

What they really do, is to `stalk and punch`:

> in relation to an offence under any other "Act or regulation", has the powers and obligations of a peace officer under ss. 495 to 497 of the Criminal Code and subsections 495(3) and 497(3) of that Act, apply to the Special Constable as if he/she is a peace officer,
> has the powers of a police officer for the purposes of ss. 16, and 17 of the Mental Health Act, R.S.O. 1990, c. M.7, as amended,
> has the powers of a police officer for the purposes of ss.31(5), 36(1), 47(1) and (1.1), and 48 of the Liquor Licence Act, R.S.O. 1990, c. L.19, as amended, and
> has the powers of a police officer for the purposes of ss. 9 of the Trespass to Property Act, R.S.O. 1990, c. T.21, as amended.

---

**TTC fare inspectors usually stick to main stations but there are a few routes they frequents:**

 - Fare cops start shift at `1138 Bathurst` and from there they head to others.
 - Spadina Station is their HQ where they change shift & prey on people at streetcar platform. The `510` and `511` routes are frequently patrolled by Fare Inspection.
 - Bathurst Station is where they prey on people at streetcar platform, usually on weekends and rush hour but also often take the `7 Bathurst` going Northbound to `St. Clair / Bathurst` to patrol the St. Clair streetcar lines. Sometime they will also get off at `Dupont`.
 - Sheppard-Yonge Station at the main entrance ticket booths and rear ticket gates
 - All terminal stations particularly Downtown Core.
 - `Union` and `Ossington` are frequent by fare cops.

**Communications of TTC Special Constable / Fare Inspector:**

Before [their transition to digital Tetra](https://www.radioreference.com/db/sid/9380) after 2019, [fare cops used analog radio service that were listenable on Broadcastify](https://web.archive.org/web/20190617050518/https://www.broadcastify.com/listen/feed/8878). At the moment, the only live feed still available is the [Toronto Transit Control](https://www.broadcastify.com/webPlayer/31629) that included management, maintenance and security, but most of their Tetra comms is independent from control talkgroups.

---
