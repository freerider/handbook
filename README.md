# Freeriders' Handbook (TTC Edition)

`Dedicated to all freeriders, particularly [TTC fare inspector lookout], [Planka.nu] and [Flea Market Socialist]`

---

## WHAT IS METROLINX

`Network of transit checkpoints for tapping targeted surveillance metro card.`

Or: *Metrolinx is a Crown agency of the Government of Ontario that manages and integrates road and public transport in the Greater Toronto and Hamilton Area (GTHA), which comprises much of Ontario's Golden Horseshoe region.*

## WHAT IS PRESTO

`Exploitable gateways to freeriding.`

Or: *Presto card is a contactless smart card automated fare collection system used on participating public transit systems in the province of Ontario, Canada, specifically in Greater Toronto, Hamilton, and Ottawa.*

## WHAT IS TTC

`Toronto Transit Commission`

## PRESTO PRIVACY 101

### [FREERIDERS' PRIVACY (TTC Edition)](./PRIVACY.md)

 - `NEVER LINK YOUR IDENTITY TO THE PRESTO CARD`
 - `PAY AND REFILL YOUR PRESTO WITH CASH`
 - `WHEN NECESSARY, SWITCH IT ONCE A WHILE`

## EXPLOITS OF TTC FREERIDING

### FILMSY DISABILITY GATES

![](https://i.ytimg.com/vi/gQzx1YcJpuY/sddefault.jpg)

#### HOW-TO VIDEO

[![](https://i.imgur.com/McfIj0Ch.jpg)](https://i.imgur.com/McfIj0C.mp4)

#### `ONE-PERSON GATE-BRUTEFORCE` (10 SEC to 1 MIN)

 1. WALK TOWARD THE GATE
 2. IGNORE THE ERROR SOUND
 3. PUT YOUR DOMINANT FOOT INSIDE THE DISABILITY GATE
 4. LEAN YOUR BODY TOWARD THE DOMINANT FOOT AND PUSH THROUGH
 5. SOME PAIN FOR FIRST-TIMER, KEEP PUSHING WHILE USE YOUR HAND TO HELP
 6. WHILE ALSO LIFT YOUR REMAINING FOOT UP AND WALK INSIDE THE GATE
 7. KEEP IGNORING THE ERROR AND KEEP WALKING.

#### `MULTI-PEOPLE GATE-BRUTEFORCE` (MUTUAL AID)

 1. WALK TOWARD THE GATE
 2. IGNORE THE ERROR SOUND
 3. PUT YOUR DOMINANT FOOT INSIDE THE DISABILITY GATE
 4. LEAN YOUR BODY TOWARD THE DOMINANT FOOT AND PUSH THROUGH
 5. SOME PAIN FOR FIRST-TIMER, KEEP PUSHING WHILE USE YOUR HAND TO HELP
 6. WHILE ALSO LIFT YOUR REMAINING FOOT UP AND WALK INSIDE THE GATE
 7. KEEP IGNORING THE ERROR
 8. ONCE INSIDE FIRST-PERSON STANDS AT THE GATE LASER SENSOR (INSIDE BLACK SQUARES)
 9. ONCE INSIDE LASER SENSOR BLOCKED THE GATE WILL KEEP OPENING WHILE ERROR BLARING
 10. KEEP IT OPEN UNTIL EVERYONE INSIDE.

#### `SCREW YOUR GATE SENSOR` (DIRECT ACTION)

 1. WALK TOWARD THE GATE WITH YOUR `[DUCT TAPE + WORK GLOVES + MASK]` PPE
 2. IGNORE THE ERROR SOUND
 3. PUT YOUR DOMINANT FOOT INSIDE THE DISABILITY GATE
 4. LEAN YOUR BODY TOWARD THE DOMINANT FOOT AND PUSH THROUGH
 5. SOME PAIN FOR FIRST-TIMER, KEEP PUSHING WHILE USE YOUR HAND TO HELP
 6. WHILE ALSO LIFT YOUR REMAINING FOOT UP AND WALK INSIDE THE GATE
 7. KEEP IGNORING THE ERROR
 8. ONCE INSIDE FIRST-PERSON STANDS AT THE GATE LASER SENSOR (INSIDE BLACK SQUARES)
 9. ONCE INSIDE LASER SENSOR BLOCKED THE GATE WILL KEEP OPENING WHILE ERROR BLARING
 10. KEEP IT OPEN UNTIL EVERYONE INSIDE
 11. RIP A SQUARE OF DUCT TAPE
 12. SLAP IT ON THE INSIDE SQUARE SENSOR TO COVER ALL OF IT
 13. PERMANENT STICKING WITH Adhesive
 14. YOU ONLY NEED TO COVER ONE INSIDE SQUARE OF SENSOR
 15. WHAT IF YOU COVER ALL SENSORS IN ALMOST EVERY STATION?
 16. ???
 17. SOLIDARITY!

---

##### BEST REAR ENTRANCE: `LAWRENCE STATION [NORTH ENRANCE]`

 - ✔️ Rare Fare Pork
 - ✔️ Low Security
 - ✔️ NIMBYs Don't Climb Staircases

![](https://i.imgur.com/uzpYswi.png)

![](https://i.imgur.com/LpsBkNe.jpg)

##### CHALLENGING REAR ENTRANCE: `[SPADINA STATION]`

 - ☠️ FARE PORK HEADQUARTER
 - ☠️ MEDIUM SECURITY
 - ✔️ TO STREETCARS - ACT CASUAL 🤫️

![](https://i.imgur.com/n3qnEny.png)

##### MULTI-REAR ENTRANCES `[SHEPPARD-YONGE STATION]`

 - ✔️ OPEN-GATE BUS STATION
 - ✔️ NORTH REAR GATE (BEHIND MCD)
 - ✔️ SOUTH REAR GATE (BEHIND PRESO TEA)
 - ✔️ HULLMARK CENTRE
 - 🤫️ FARE PORK FREQUENTS
 - ☠️ MEDIUM SECURITY

![](https://i.imgur.com/BhN1Nbx.png)

### TAILGATING

#### How to tailgate the Presto Subway Gates:

 1. Wait for a person to tap their Presto card
 2. As soon as the gates open walk behind that person
 3. Keep following them until you get inside
 4. As long as the sensor blocked or gate forced open
 5. Tailgating works with multi-people.

##### `Image link is now a video`

[![](https://i.imgur.com/hQTMM6w.jpg)](https://video.twimg.com/amplify_video/1101546133594230785/vid/200x358/ks3nSj1WFTUGZfVm.mp4)

#### How to tailgate the Buses:

`@iamprince_rg` (✊🏽️)

[![](https://i.imgur.com/K44CRcn.jpg)](https://www.tiktok.com/@iamprince_rg/video/7110337893394124037)

 1. Wait for someone to get off the bus rear door
 2. As soon as the person leave hold the rear door to keep it open
 3. Let yourself into the bus and the forced rear door will close on its own
 4. Tip works better and covert in rush hours because driver would be too busy to mind.

### OTHER TECHNIQUES

#### CHILD PASS

[Presto Child Pass](https://www.prestocard.ca/en/about/using-presto#faretype) is unlimited traveling time because of the transit loophole that allow children under ages of 12 to ride for free and they can be obtained at Shopper Drug Mart.

#### Doggo's Sensor Blocking

 1. Train doggo friend to get in and block the inside square sensor
 2. As long as doggo stay block the sensor you can get inside
 3. Pet the doggo and give your friend some treat.

![](https://i.cbc.ca/1.5033762.1551194322!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/dog-evading-ttc-fare.jpg)

#### Using The Dangling Bag

 1. Dangle your day bag on the other side of the gate to block out the square sensor
 2. Walk in when gates open
 3. ???
 4. Profit!

![](https://i.ytimg.com/vi/aLc42Z-58_A/maxresdefault.jpg)

#### USING FOOT

 1. Put a foot to the other side of the gates until parallel with the square sensor
 2. Once square sensor blocked out the gates will open
 3. This technique worked when they first rolled out the Presto gates
 4. Still works in some gates but longer sensor timeout has been setup to prevent this
 5. https://player.vimeo.com/video/271581411?h=cff03c7e7

## EVADING FARE INSPECTORS

![](https://i.imgur.com/GtczUPt.jpeg)

**TTC fare inspectors usually stick to main stations but there are a few routes they frequents:**

 - Fare cops start shift at `1138 Bathurst` and from there they head to others.
 - Spadina Station is their HQ where they change shift & prey on people at streetcar platform. The `510` and `511` routes are frequently patrolled by Fare Inspection.
 - Bathurst Station is where they prey on people at streetcar platform, usually on weekends and rush hour but also often take the `7 Bathurst` going Northbound to `St. Clair / Bathurst` to patrol the St. Clair streetcar lines. Sometime they will also get off at `Dupont`.
 - Sheppard-Yonge Station at the main entrance ticket booths and rear ticket gates
 - All terminal stations particularly Downtown Core.
 - `Union` and `Ossington` are frequent by fare cops.

**Communications of TTC Special Constable / Fare Inspector:**

Before [their transition to digital Tetra](https://www.radioreference.com/db/sid/9380) after 2019, [fare cops used analog radio service that were listenable on Broadcastify](https://web.archive.org/web/20190617050518/https://www.broadcastify.com/listen/feed/8878). At the moment, the only live feed still available is the [Toronto Transit Control](https://www.broadcastify.com/webPlayer/31629) that included management, maintenance and security, but most of their Tetra comms is independent from control talkgroups.

### STRATEGIES WHEN ENCOUNTERING FARE COPS

#### EXITING STREETCARS

When a streetcar arrived at a station platform fare cops (in pair of two or three) will block the first exit of the streetcar nearest to the station platform, if they patrol in three the third cop will block the middle exit. **That leaves the furthest exits free from their reach.** Act casual but walk quickly and blend with the crowd. Run when you need to.

#### RIDING STREETCARS

On rare occasions they will check for ticket on the transit just to fuck with people. When this happens, exit the vehicle at the nearest stop, with the closest exit doors you can find. TTC streetcar doors don't automatically open unless button pressed, or when it arrives at a stop, you will have to long press the door button beforehand to make it open as soon as the streetcar arrived at a stop. Run when you need to.

#### TRANSFER AS TEMPORARY PASS

![](https://images.thestar.com/bG7X-3v1M3jqUQkREAu5P0i9G_k=/480x335/smart/filters:cb(2700061000):format(webp)/https://www.thestar.com/content/dam/thestar/news/gta/2019/01/03/on-new-years-eve-this-man-visited-all-75-ttc-subway-stations-in-less-than-10-hours-he-says-museum-is-his-favourite/_1_sheppard_west.jpg)

Often people leave [paper transfers](https://www.ttc.ca/fares-and-passes/fare-information/transfers) on the ground or chair when they finished using them. These paper transfers have half/one-hour validity from the time of its purchase. Fare cops sometime will waive you pass when they don't check the validity of the transfers except on rush hour. [A list of transfer locations.](https://www.ttc.ca/Fares-and-passes/Fare-information/Transfers/Walking-Transfer-Locations)

## HACKING PRESTO

### RELAY ATTACK (NFC)

#### WITH PHONE

![](https://i.imgur.com/Uj3VLo6.png)

What you need:

 - `NFC TOOLS` https://play.google.com/store/apps/details?id=com.wakdev.wdnfc
 - `NFC ReTag` https://play.google.com/store/apps/details?id=com.widgapp.NFC_ReTAG_FREE
 - `NFC ReTag Expert Plugin` https://play.google.com/store/apps/details?id=com.widgapp.NFC_ReTag_Expert_Plugin
 - DNS Ad Blocker like AdGuard (for ReTag)

#### WITH TRANSCEIVERS

![](https://blog.flipperzero.one/content/images/2021/09/Untitled-183831--1-.jpg)

What you need:

 - Presto uses `MIFARE DESFire EV1 4k` for NFC, which operates at 13.56 MHz.
 - `MIFARE DESFire EV1` DOCUMENTATIONS: [1](https://web.archive.org/web/20220708142053/https://faremi.keybase.pub/MIFARE-DESFire-EV1-Product-Flyer-Web.pdf), [2](https://web.archive.org/web/20220708142147/https://faremi.keybase.pub/MF3ICDQ1_MF3ICDHQ1_SDS.pdf), [3](https://web.archive.org/web/20220708142231/https://faremi.keybase.pub/MF3ICDX21_41_81_SDS.pdf) and [4](https://web.archive.org/web/20220215170710/https://www.hidglobal.com/sites/default/files/resource_files/mifare-desfire-ev1-card-ds-en.pdf)
 - NFC/SDR transceivers. The recommended budget open-source tool for this is `Flipper Zero`.
 - Proxmark3 is pricier option but offers more freedom to exploit Presto freqs.
 - Blank magnetic cards or programmable key fobs with equivalent frequencies range
 - Or you could build an [Arduino NFC reader](https://www.allaboutcircuits.com/projects/rfid-technology-using-arduino-mega/)

How this works:

 1. Find your target's card
 2. Capture the signal with the NFC reader
 3. Clone the signal to programmable card/key fob
 4. Confirm your cloned batch.

### PRESTO DENIAL-OF-SERVICE

`THIS 0DAY IS WORK-IN-PROGRESS, FOR EDUCATIONAL PURPOSE ONLY`

There is a bug in Presto machine that can crash all other machines on the same cluster. If you have a Proxmark3 or Flipper Zero, you can program the Presto card with null or blank input, but with valid send signal. Presto machine use `Microsoft Windows CE 7.0` and is still possible vulnerable to something like CVE-2006-6908 with BoF denial-of-service. Not every Presto machine OS gets patched regularly.

The test way would by programming the null inputs or exploit payloads into the card signal, tap the card on a bus Presto machine and there is two ways this gonna crash: if the card get green light, this would be silent DoS, it won't show the error until the next person tap their valid card. If it shows error your card either worked or the command was denied.
